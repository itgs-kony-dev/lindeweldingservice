﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using LindeWeldingService.DataObjects;
using LindeWeldingService.Models;

namespace LindeWeldingService.Controllers
{
    public class CrossSectionController : TableController<CrossSection>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            LindeWeldingContext context = new LindeWeldingContext();
            DomainManager = new EntityDomainManager<CrossSection>(context, Request);
        }

        // GET tables/CrossSection
        public IQueryable<CrossSection> GetAllCrossSections()
        {
            return Query();
        }

        // GET tables/CrossSection/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<CrossSection> GetCrossSection(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/CrossSection/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<CrossSection> PatchCrossSection(string id, Delta<CrossSection> patch)
        {
            return UpdateAsync(id, patch);
        }

        // POST tables/CrossSection
        public async Task<IHttpActionResult> PostCrossSection(CrossSection item)
        {
            CrossSection current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/CrossSection/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteCrossSection(string id)
        {
            return DeleteAsync(id);
        }
    }
}