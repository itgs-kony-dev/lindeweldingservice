﻿using LindeWeldingService.DataObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.WindowsAzure.Storage.File;
using System.Web.Http;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Azure.Mobile.Server.Files.Controllers;
using Microsoft.Azure.Mobile.Server.Files;

namespace LindeWeldingSerice.Controllers
{
    public class CrossSectionStorageController : StorageController<CrossSection>
    {
        public CrossSectionStorageController()
        {

        }
        [HttpPost]
        [Route("tables/CrossSection/{id}/StorageToken")]
        public async Task<HttpResponseMessage> PostStorageTokenRequest(string id, StorageTokenRequest value)
        {
            StorageToken token = await GetStorageTokenAsync(id, value);

            return Request.CreateResponse(token);
        }

        // Get the files associated with this record
        [HttpGet]
        [Route("tables/CrossSection/{id}/MobileServiceFiles")]
        public async Task<HttpResponseMessage> GetFiles(string id)
        {
            IEnumerable<MobileServiceFile> files = await GetRecordFilesAsync(id);

            return Request.CreateResponse(files);
        }

        [HttpDelete]
        [Route("tables/CrossSection/{id}/MobileServiceFiles/{name}")]
        public Task Delete(string id, string name)
        {
            return base.DeleteFileAsync(id, name);
        }
    }
}