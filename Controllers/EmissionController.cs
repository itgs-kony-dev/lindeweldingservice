﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using LindeWeldingService.DataObjects;
using LindeWeldingService.Models;

namespace LindeWeldingService.Controllers
{
    public class EmissionController : TableController<Emission>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            LindeWeldingContext context = new LindeWeldingContext();
            DomainManager = new EntityDomainManager<Emission>(context, Request);
        }

        // GET tables/Emission
        public IQueryable<Emission> GetAllEmissions()
        {
            return Query();
        }

        // GET tables/Emission/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<Emission> GetEmission(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/Emission/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<Emission> PatchEmission(string id, Delta<Emission> patch)
        {
            return UpdateAsync(id, patch);
        }

        // POST tables/Emission
        public async Task<IHttpActionResult> PostEmission(Emission item)
        {
            Emission current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/Emission/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteEmission(string id)
        {
            return DeleteAsync(id);
        }
    }
}