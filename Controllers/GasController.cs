﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using LindeWeldingService.DataObjects;
using LindeWeldingService.Models;

namespace LindeWeldingService.Controllers
{
    public class GasController : TableController<Gas>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            LindeWeldingContext context = new LindeWeldingContext();
            DomainManager = new EntityDomainManager<Gas>(context, Request);
        }

        // GET tables/Gas
        public IQueryable<Gas> GetAllGases()
        {
            return Query();
        }

        // GET tables/Gas/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<Gas> GetGas(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/Gas/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<Gas> PatchGas(string id, Delta<Gas> patch)
        {
            return UpdateAsync(id, patch);
        }

        // POST tables/Gas
        public async Task<IHttpActionResult> PostGas(Gas item)
        {
            Gas current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/Gas/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteGas(string id)
        {
            return DeleteAsync(id);
        }
    }
}