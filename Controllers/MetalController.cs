﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using LindeWeldingService.DataObjects;
using LindeWeldingService.Models;

namespace LindeWeldingService.Controllers
{
    public class MetalController : TableController<Metal>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            LindeWeldingContext context = new LindeWeldingContext();
            DomainManager = new EntityDomainManager<Metal>(context, Request);
        }

        // GET tables/Metal
        public IQueryable<Metal> GetAllMetals()
        {
            return Query();
        }

        // GET tables/Metal/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<Metal> GetMetal(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/Metal/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<Metal> PatchMetal(string id, Delta<Metal> patch)
        {
            return UpdateAsync(id, patch);
        }

        // POST tables/Metal
        public async Task<IHttpActionResult> PostMetal(Metal item)
        {
            Metal current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/Metal/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteMetal(string id)
        {
            return DeleteAsync(id);
        }
    }
}