﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using LindeWeldingService.DataObjects;
using LindeWeldingService.Models;

namespace LindeWeldingService.Controllers
{
    public class Model3DScanController : TableController<Model3DScan>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            LindeWeldingContext context = new LindeWeldingContext();
            DomainManager = new EntityDomainManager<Model3DScan>(context, Request);
        }

        // GET tables/Model3DScan
        public IQueryable<Model3DScan> GetAllModel3DScans()
        {
            return Query();
        }

        // GET tables/Model3DScan/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<Model3DScan> GetModel3DScan(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/Model3DScan/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<Model3DScan> PatchModel3DScan(string id, Delta<Model3DScan> patch)
        {
            return UpdateAsync(id, patch);
        }

        // POST tables/Model3DScan
        public async Task<IHttpActionResult> PostModel3DScan(Model3DScan item)
        {
            Model3DScan current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/Model3DScan/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteModel3DScan(string id)
        {
            return DeleteAsync(id);
        }
    }
}