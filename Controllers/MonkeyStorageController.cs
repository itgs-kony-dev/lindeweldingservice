﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
//using Microsoft.WindowsAzure.Mobile.Service.Files;
//using Microsoft.WindowsAzure.MobileServices.Files;
//using Microsoft.WindowsAzure.MobileServices.Files.Controllers;
using Microsoft.Azure.Mobile.Server.Files.Controllers;
using Microsoft.Azure.Mobile.Server.Files;
using LindeWeldingService.DataObjects;

namespace LindeWeldingService.Controllers
{
    public class MonkeyStorageController : StorageController<Monkey>
    {
        public MonkeyStorageController()
        {

        }
        [HttpPost]
        [Route("tables/Monkey/{id}/StorageToken")]
        public async Task<HttpResponseMessage> PostStorageTokenRequest(string id, StorageTokenRequest value)
        {
            StorageToken token = await GetStorageTokenAsync(id, value);

            return Request.CreateResponse(token);
        }

        // Get the files associated with this record
        [HttpGet]
        [Route("tables/Monkey/{id}/MobileServiceFiles")]
        public async Task<HttpResponseMessage> GetFiles(string id)
        {
            IEnumerable<MobileServiceFile> files = await GetRecordFilesAsync(id);

            return Request.CreateResponse(files);
        }

        [HttpDelete]
        [Route("tables/Monkey/{id}/MobileServiceFiles/{name}")]
        public Task Delete(string id, string name)
        {
            return base.DeleteFileAsync(id, name);
        }
    }
}