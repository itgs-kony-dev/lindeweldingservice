﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using LindeWeldingService.DataObjects;
using LindeWeldingService.Models;

namespace LindeWeldingService.Controllers
{
    public class ProcessController : TableController<Process>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            LindeWeldingContext context = new LindeWeldingContext();
            DomainManager = new EntityDomainManager<Process>(context, Request);
        }

        // GET tables/Process
        public IQueryable<Process> GetAllProcesses()
        {
            return Query();
        }

        // GET tables/Process/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<Process> GetProcess(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/Process/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<Process> PatchProcess(string id, Delta<Process> patch)
        {
            return UpdateAsync(id, patch);
        }

        // POST tables/Process
        public async Task<IHttpActionResult> PostProcess(Process item)
        {
            Process current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/Process/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteProcess(string id)
        {
            return DeleteAsync(id);
        }
    }
}