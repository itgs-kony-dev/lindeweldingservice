﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using LindeWeldingService.DataObjects;
using LindeWeldingService.Models;

namespace LindeWeldingService.Controllers
{
    public class TechnicalDataController : TableController<TechnicalData>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            LindeWeldingContext context = new LindeWeldingContext();
            DomainManager = new EntityDomainManager<TechnicalData>(context, Request);
        }

        // GET tables/TechnicalData
        public IQueryable<TechnicalData> GetAllTechnicalDatas()
        {
            return Query();
        }

        // GET tables/TechnicalData/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<TechnicalData> GetTechnicalData(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/TechnicalData/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<TechnicalData> PatchTechnicalData(string id, Delta<TechnicalData> patch)
        {
            return UpdateAsync(id, patch);
        }

        // POST tables/TechnicalData
        public async Task<IHttpActionResult> PostTechnicalData(TechnicalData item)
        {
            TechnicalData current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/TechnicalData/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteTechnicalData(string id)
        {
            return DeleteAsync(id);
        }
    }
}