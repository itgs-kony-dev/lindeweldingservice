﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using LindeWeldingService.DataObjects;
using LindeWeldingService.Models;

namespace LindeWeldingService.Controllers
{
    public class WikiDataController : TableController<WikiData>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            LindeWeldingContext context = new LindeWeldingContext();
            DomainManager = new EntityDomainManager<WikiData>(context, Request);
        }

        // GET tables/WikiData
        public IQueryable<WikiData> GetAllWikiDatas()
        {
            return Query();
        }

        // GET tables/WikiData/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<WikiData> GetWikiData(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/WikiData/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<WikiData> PatchWikiData(string id, Delta<WikiData> patch)
        {
            return UpdateAsync(id, patch);
        }

        // POST tables/WikiData
        public async Task<IHttpActionResult> PostWikiData(WikiData item)
        {
            WikiData current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/WikiData/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteWikiData(string id)
        {
            return DeleteAsync(id);
        }
    }
}