﻿using Microsoft.Azure.Mobile.Server;

namespace LindeWeldingService.DataObjects
{
    public class CrossSection : EntityData
    {
        public string Metal { get; set; }
        public string Process { get; set; }
        public string Gas { get; set; }
        public string Filename { get; set; }
        public string Type { get; set; }
    }
}