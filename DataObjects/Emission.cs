﻿using Microsoft.Azure.Mobile.Server;

namespace LindeWeldingService.DataObjects
{
    public class Emission : EntityData
    {
        public string Metal { get; set; }
        public string Process { get; set; }
        public string Gas { get; set; }
        public string Speed { get; set; }
        public string Speed_Unit { get; set; }

        public double min { get; set; }
        public double max { get; set; }
        public double _10A { get; set; }
        public double MaxScale { get; set; }

        public string Emission_type { get; set; }
        public double emission { get; set; }
        public string Emission_Unit { get; set; }
    }
}