﻿using Microsoft.Azure.Mobile.Server;

namespace LindeWeldingService.DataObjects
{
    public class Gas : EntityData
    {
        public string Name { get; set; }

        public bool IsPremium { get; set; }
    }
}