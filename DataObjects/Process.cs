﻿using Microsoft.Azure.Mobile.Server;

namespace LindeWeldingService.DataObjects
{
    public class Process : EntityData
    {
        public string Name { get; set; }
    }
}