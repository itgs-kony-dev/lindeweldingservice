﻿using Microsoft.Azure.Mobile.Server;

namespace LindeWeldingService.DataObjects
{
    public class WikiData : EntityData
    {
        public string Metal { get; set; }
        public string Process { get; set; }
        public string Gas { get; set; }
        public string Filename { get; set; }
    }
}