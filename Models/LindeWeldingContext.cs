﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using Microsoft.Azure.Mobile.Server;
using Microsoft.Azure.Mobile.Server.Tables;
using LindeWeldingService.DataObjects;

namespace LindeWeldingService.Models
{
    public class LindeWeldingContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to alter your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx

        private const string connectionStringName = "Name=MS_TableConnectionString";

        public LindeWeldingContext() : base(connectionStringName)
        {
        } 

        public DbSet<TodoItem> TodoItems { get; set; }
        public DbSet<Metal> Metals { get; set; }
        public DbSet<Emission> Emissions { get; set; }
        public DbSet<Gas> Gases { get; set; }
        public DbSet<Process> Processes { get; set; }
        public DbSet<CrossSection> CrossSections { get; set; }
        public DbSet<WikiData> WikiData { get; set; }
        public DbSet<TechnicalData> TechnicalData { get; set; }
        public DbSet<Model3DScan> Model3DScans { get; set; }

        public DbSet<Monkey> Monkeys { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Add(
                new AttributeToColumnAnnotationConvention<TableColumnAttribute, string>(
                    "ServiceTableColumn", (property, attributes) => attributes.Single().ColumnType.ToString()));
        }
    }

}
