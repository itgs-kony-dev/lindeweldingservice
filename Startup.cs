using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(LindeWeldingService.Startup))]

namespace LindeWeldingService
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureMobileApp(app);
        }
    }
}